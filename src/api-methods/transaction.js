const WebApiRequest = require('../webapi-request');
const HttpManager = require('../http-manager');

function TransactionMethods(accessToken, apiUrl) {
  return {
    /**
     * GET all transactions for a specific wallet.
     * @param {Int} [walletId] The wallet's ID.
     * @param {requestCallback} [callback] Optional callback method to be called instead of the promise.
     * @returns {Promise|undefined}
     */
    getTransactionsForWallet: (walletId, callback) => {
      const params = { walletId };
      return WebApiRequest.builder(accessToken, apiUrl)
        .setPath(`/transactions`)
        .setQueryParameters(params)
        .execute(HttpManager.get, callback);
    },
  };
}

module.exports = TransactionMethods;
