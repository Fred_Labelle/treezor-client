const WebApiRequest = require('../webapi-request');
const HttpManager = require('../http-manager');

function UserMethods(accessToken, apiUrl) {
  return {
    /**
     * List all users associated on a given account.
     * @param {requestCallback} [callback] Optional callback method to be called instead of the promise.
     * @returns {Promise|undefined}
     */
    getUsers: callback =>
      WebApiRequest.builder(accessToken, apiUrl)
        .setPath(`/users`)
        .execute(HttpManager.get, callback, 'users'),

    /**
     * Look up user info with ID.
     * @param {String} userId
     * @param {requestCallback} [callback] Optional callback method to be called instead of the promise.
     * @returns {Promise|undefined}
     */
    getUser: (userId, callback) =>
      WebApiRequest.builder(accessToken, apiUrl)
        .setPath(`/users/${userId}`)
        .execute(HttpManager.get, callback, 'users'),

    /**
     * Create user.
     * @param {Object} Input User info input, mandatory: email and User type.
     * @param {requestCallback} [callback] Optional callback method to be called instead of the promise.
     * @returns {Promise|undefined}
     */
    createUser: (user, callback) =>
      WebApiRequest.builder(accessToken, apiUrl)
        .setPath(`/users`)
        .setQueryParameters(user)
        .execute(HttpManager.post, callback, 'users'),

    /**
     * Update user.
     * @param {Object} Input User info input
     * @param {requestCallback} [callback] Optional callback method to be called instead of the promise.
     * @returns {Promise|undefined}
     */
    updateUser: (user, callback) =>
      WebApiRequest.builder(accessToken, apiUrl)
        .setPath(`/users/${user.userId}`)
        .setQueryParameters(user)
        .execute(HttpManager.put, callback, 'users'),

    /**
     * Launch User KYC.
     * @param {String} userId
     * @param {requestCallback} [callback] Optional callback method to be called instead of the promise.
     * @returns {Promise|undefined}
     */
    launchUserKyc: (userId, callback) =>
      WebApiRequest.builder(accessToken, apiUrl)
        .setPath(`/users/${userId}/Kycreview/`)
        .execute(HttpManager.put, callback),
  };
}

module.exports = UserMethods;
