const WebApiRequest = require('../webapi-request');
const HttpManager = require('../http-manager');

function TransferMethods(accessToken, apiUrl) {
  return {
    /**
     * Get all transfers from all wallets
     * @param {requestCallback} [callback] Optional callback method to be called instead of the promise.
     * @returns {Promise|undefined}
     */
    getTransfers: callback =>
      WebApiRequest.builder(accessToken, apiUrl)
        .setPath(`/transfers`)
        .execute(HttpManager.get, callback, 'transfers'),
    /**
     * GET all transfers for a specific wallet.
     * @param {Int} [walletId] The wallet's ID.
     * @param {requestCallback} [callback] Optional callback method to be called instead of the promise.
     * @returns {Promise|undefined}
     */
    getTransferForWallet: (walletId, callback) => {
      const params = {
        beneficiaryWalletId: walletId,
      };
      return WebApiRequest.builder(accessToken, apiUrl)
        .setPath(`/transfers`)
        .setQueryParameters(params)
        .execute(HttpManager.get, callback, 'transfers');
    },
  };
}

module.exports = TransferMethods;
