const WebApiRequest = require('../webapi-request');
const HttpManager = require('../http-manager');

function PayoutMethods(accessToken, apiUrl) {
  return {
    /**
     * GET all payouts for a specific wallet.
     * @param {Int} [walletId] The wallet's ID.
     * @param {requestCallback} [callback] Optional callback method to be called instead of the promise.
     * @returns {Promise|undefined}
     */
    getPayoutsForWallet: (walletId, callback) => {
      const params = { walletId };
      return WebApiRequest.builder(accessToken, apiUrl)
        .setPath(`/payouts`)
        .setQueryParameters(params)
        .execute(HttpManager.get, callback, 'payouts');
    },

    /**
     * Create a payout.
     * @param {Int} [walletId]
     * @param {Int} [bankaccountId]
     * @param {Int} [amount]
     * @param {String} [currency] ISO 4217 - ex: EUR
     * @param {Object} [params] The possible params.
     * @param {requestCallback} [callback] Optional callback method to be called instead of the promise.
     * @returns {Promise|undefined}
     */
    createPayoutsFromWalletToBankAccount: (
      walletId,
      bankaccountId,
      amount,
      currency,
      params,
      callback,
    ) => {
      const options = {
        walletId,
        bankaccountId,
        amount,
        currency,
      };
      const input = Object.assign(options, params);
      return WebApiRequest.builder(accessToken, apiUrl)
        .setPath(`/payouts`)
        .setQueryParameters(input)
        .execute(HttpManager.post, callback, 'payouts');
    },

    /**
     * Cancel Payout - Change pay out status to CANCELED. A VALIDATED pay out can't be canceled.
     * @param {Int} [payoutId]
     * @param {requestCallback} [callback] Optional callback method to be called instead of the promise.
     * @returns {Promise|undefined}
     */
    cancelPayout: (payoutId, callback) =>
      WebApiRequest.builder(accessToken, apiUrl)
        .setPath(`/payouts/${payoutId}`)
        .execute(HttpManager.del, callback),
  };
}

module.exports = PayoutMethods;
