const WebApiRequest = require('../webapi-request');
const HttpManager = require('../http-manager');

function WalletMethods(accessToken, apiUrl) {
  return {
    /**
     * Look up user's wallets.
     * @param {Object} [params] The possible params.
     * @param {requestCallback} [callback] Optional callback method to be called instead of the promise.
     * @example getWallets().then(...)
     * @returns {Promise|undefined} A promise that if successful, returns an object containing information
     *          about the wallets. Not returned if a callback is given.
     */
    getWallets: (params, callback) =>
      WebApiRequest.builder(accessToken, apiUrl)
        .setPath('/wallets')
        .setQueryParameters(params)
        .execute(HttpManager.get, callback, 'wallets'),
    /**
     * GET wallet information with ID.
     * @param {Int} [walletId] The wallet's ID.
     * @param {requestCallback} [callback] Optional callback method to be called instead of the promise.
     * @returns {Promise|undefined}
     */
    getWallet: (walletId, callback) =>
      WebApiRequest.builder(accessToken, apiUrl)
        .setPath(`/wallets/${walletId}`)
        .execute(HttpManager.get, callback, 'wallets'),
    /**
     * Create wallet for user.
     * @param {Int} [walletTypeId]
     * @param {Int} [tariffId]
     * @param {Int} [userId] Wallet's owner user id.
     * @param {String} [currency] Wallet currency, ISO 4217 3-letter code for each currency : Euro = EUR ; US Dollar = USD.
     * @param {String} [wallletName]
     * @param {Object} [params] The possible params.
     * @param {requestCallback} [callback] Optional callback method to be called instead of the promise.
     * @returns {Promise|undefined}
     */
    createWallet: (
      walletTypeId,
      tariffId,
      userId,
      currency,
      wallletName,
      params,
      callback,
    ) => {
      const actualOptions = {
        walletTypeId,
        tariffId,
        userId,
        currency,
        eventName: wallletName,
      };

      if (typeof params === 'object') {
        Object.keys(params).forEach(key => {
          actualOptions[key] = params[key];
        });
      }

      return WebApiRequest.builder(accessToken, apiUrl)
        .setPath(`/wallets`)
        .setBodyParameters(actualOptions)
        .execute(HttpManager.post, callback, 'wallets');
    },

    /**
     * Delete wallet with ID.
     * @param {Int} [walletId]
     * @param {Int} [origin] Request's origin. Possible values are: OPERATOR, USER.
     * @param {requestCallback} [callback] Optional callback method to be called instead of the promise.
     * @returns {Promise|undefined}
     */
    deleteWallet: (walletId, origin, callback) => {
      const params = {
        origin,
      };

      return WebApiRequest.builder(accessToken, apiUrl)
        .setPath(`/wallets/${walletId}`)
        .setQueryParameters(params)
        .execute(HttpManager.del, callback);
    },
  };
}

module.exports = WalletMethods;
