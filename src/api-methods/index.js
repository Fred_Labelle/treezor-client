const WalletMethods = require('./wallet');
const TransactionMethods = require('./transaction');
const UserMethods = require('./user');
const PayinMethos = require('./payin');
const PayoutMethods = require('./payout');
const TransferMethods = require('./transfer');
const ServerMethods = require('./server');
const BankAccountMethods = require('./bank-account');
const DocumentMethods = require('./document');

function ApiMethods(accessToken, apiUrl) {
  const walletMethods = new WalletMethods(accessToken, apiUrl);
  const transactionMethods = new TransactionMethods(accessToken, apiUrl);
  const userMethods = new UserMethods(accessToken, apiUrl);
  const payinMethods = new PayinMethos(accessToken, apiUrl);
  const payoutMethods = new PayoutMethods(accessToken, apiUrl);
  const transferMethods = new TransferMethods(accessToken, apiUrl);
  const serverMethods = new ServerMethods(accessToken, apiUrl);
  const bankAccountMethods = new BankAccountMethods(accessToken, apiUrl);
  const documentMethods = new DocumentMethods(accessToken, apiUrl);

  return Object.assign(
    walletMethods,
    transactionMethods,
    userMethods,
    payinMethods,
    payoutMethods,
    transferMethods,
    serverMethods,
    bankAccountMethods,
    documentMethods,
  );
}

module.exports = ApiMethods;
