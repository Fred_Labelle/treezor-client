// DEPECRECATED - Use BENEFICIARIES

const WebApiRequest = require('../webapi-request');
const HttpManager = require('../http-manager');

function PayinMethods(accessToken, apiUrl) {
  return {
    /**
     * GET all external bank account added as beneficiary, for all users.
     * @param {Object} [params] Optionnal params.
     * @param {requestCallback} [callback] Optional callback method to be called instead of the promise.
     * @returns {Promise|undefined}
     */
    getBankAccounts: (params, callback) =>
      WebApiRequest.builder(accessToken, apiUrl)
        .setPath(`/bankaccounts`)
        .setQueryParameters(params)
        .execute(HttpManager.get, callback, 'bankaccounts'),
    /**
     * POST create beneficiary Bank Account for user
     * @param {Object} [params] Optionnal params.
     * @param {requestCallback} [callback] Optional callback method to be called instead of the promise.
     * @returns {Promise|undefined}
     */
    createBankAccount: (
      userId,
      bankaccountOwnerName,
      bankaccountOwnerAddress,
      bankaccountIBAN,
      bankaccountBIC,
      bankaccountType,
      params,
      callback,
    ) => {
      const options = {
        userId,
        bankaccountOwnerName,
        bankaccountOwnerAddress,
        bankaccountIBAN,
        bankaccountBIC,
        bankaccountType,
      };
      const input = Object.assign(options, params);
      return WebApiRequest.builder(accessToken, apiUrl)
        .setPath(`/bankaccounts`)
        .setQueryParameters(input)
        .execute(HttpManager.post, callback);
    },

    /**
     * GET  beneficiary Bank Account info
     * @param {Int} bankAccountId Optionnal params.
     * @param {requestCallback} [callback] Optional callback method to be called instead of the promise.
     * @returns {Promise|undefined}
     */
    getBankAccountWithId: (bankaccountId, callback) =>
      WebApiRequest.builder(accessToken, apiUrl)
        .setPath(`/bankaccounts/${bankaccountId}`)
        .execute(HttpManager.get, callback, 'bankaccounts'),
    /**
     * DELETE  beneficiary Bank Account with ID
     * @param {Int} bankAccountId Optionnal params.
     * @param {requestCallback} [callback] Optional callback method to be called instead of the promise.
     * @returns {Promise|undefined}
     */
    deleteBankAccountWithId: (bankaccountId, callback) =>
      WebApiRequest.builder(accessToken, apiUrl)
        .setPath(`/bankaccounts/${bankaccountId}`)
        .execute(HttpManager.del, callback),
  };
}

module.exports = PayinMethods;
