const WebApiRequest = require('../webapi-request');
const HttpManager = require('../http-manager');

function DocumentMethods(accessToken, apiUrl) {
  return {
    getDocuments: (params, callback) =>
      WebApiRequest.builder(accessToken, apiUrl)
        .setPath('/documents')
        .setQueryParameters(params)
        .execute(HttpManager.get, callback, 'documents'),

    getDocument: (documentId, callback) =>
      WebApiRequest.builder(accessToken, apiUrl)
        .setPath(`/documents/${documentId}`)
        .execute(HttpManager.get, callback, 'documents'),

    createDocument: (input, callback) =>
      WebApiRequest.builder(accessToken, apiUrl)
        .setPath(`/documents`)
        .setBodyParameters({ ...input })
        .execute(HttpManager.post, callback, 'documents'),

    deleteDocument: (documentId, callback) =>
      WebApiRequest.builder(accessToken, apiUrl)
        .setPath(`/documents/${documentId}`)
        .execute(HttpManager.del, callback),
  };
}

module.exports = DocumentMethods;
