const WebApiRequest = require('../webapi-request');
const HttpManager = require('../http-manager');

function ServerMethods(accessToken, apiUrl) {
  return {
    /**
     * Ping server.
     * @param {requestCallback} [callback] Optional callback method to be called instead of the promise.
     * @returns {Promise|undefined} A promise that if successful, returns an object containing information
     *          about the wallets. Not returned if a callback is given.
     */
    heartBeat: callback =>
      WebApiRequest.builder(accessToken, apiUrl)
        .setPath('/heartbeats')
        .execute(HttpManager.get, callback),
  };
}

module.exports = ServerMethods;
