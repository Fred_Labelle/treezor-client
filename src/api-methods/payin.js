const WebApiRequest = require('../webapi-request');
const HttpManager = require('../http-manager');

function PayinMethods(accessToken, apiUrl) {
  return {
    /**
     * GET all payins for a specific wallet.
     * @param {Int} [walletId] The wallet's ID.
     * @param {requestCallback} [callback] Optional callback method to be called instead of the promise.
     * @returns {Promise|undefined}
     */
    getPayinsForWallet: (walletId, callback) => {
      const params = { walletId };
      return WebApiRequest.builder(accessToken, apiUrl)
        .setPath(`/payins`)
        .setQueryParameters(params)
        .execute(HttpManager.get, callback, 'payins');
    },
  };
}

module.exports = PayinMethods;
