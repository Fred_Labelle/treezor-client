class Request {
  constructor() {
    this.host = '';
    this.port = 443;
    this.scheme = '';
    this.queryParameters = {};
    this.bodyParameters = {};
    this.headers = {};
    this.path = '';
  }

  getHost() {
    return this.host;
  }

  getPort() {
    return this.port;
  }

  getScheme() {
    return this.scheme;
  }

  getPath() {
    return this.path;
  }

  getQueryParameters() {
    return this.queryParameters;
  }

  getBodyParameters() {
    return this.bodyParameters;
  }

  getHeaders() {
    return this.headers;
  }

  getURI() {
    if (!this.scheme || !this.host || !this.port) {
      throw new Error('Missing components necessary to construct URI');
    }
    let uri = `${this.scheme}://${this.host}`;
    if (
      (this.scheme === 'http' && this.port !== 80)
      || (this.scheme === 'https' && this.port !== 443)
    ) {
      uri += `: ${this.port}`;
    }
    if (this.path) {
      uri += this.path;
    }
    return uri;
  }

  getURL() {
    const uri = this.getURI();
    if (this.getQueryParameters()) {
      return uri + this.getQueryParameterString(this.getQueryParameters());
    }
    return uri;
  }

  getQueryParameterString() {
    const queryParameters = this.getQueryParameters();
    if (queryParameters) {
      const concatenatedParams = Object.keys(queryParameters)
      .filter(key => queryParameters[key] !== undefined)
      .map(key => `${key}=${queryParameters[key]}`)
      .join('&');
      return `?${concatenatedParams}`;
    }
    return '';
  }

  setHost(host) {
    this.host = host;
    return this;
  }

  setPort(port) {
    this.port = port;
    return this;
  }

  setScheme(scheme) {
    this.scheme = scheme;
    return this;
  }

  setPath(path) {
    this.path = path;
    return this;
  }

  assigner(key, params) {
    this[key] = this.assign(this[key], params);
    return this;
  }

  assign(src, obj) {
    if (obj && Object.keys(obj).length > 0) {
      return Object.assign(src || {}, obj);
    }
    return src;
  }

  setQueryParameters(queryParameters) {
    return this.assigner('queryParameters', queryParameters);
  }

  setBodyParameters(bodyParameters) {
    return this.assigner('bodyParameters', bodyParameters);
  }

  setHeaders(headers) {
    return this.assigner('headers', headers);
  }

  setAuth(accessToken) {
    if (accessToken) {
      this.setHeaders({ Authorization: `Bearer ${accessToken}` });
    }
    return this;
  }

  execute(method, callback, bodyParsingKey) {
    // To log request, uncomment :
    // console.info(this);
    if (callback) {
      method(this, callback, bodyParsingKey);
      return;
    }
    const _self = this;

    return new Promise((resolve, reject) => {
      method(_self, (error, result) => {
        if (error) {
          reject(error);
        } else {
          if(bodyParsingKey && !result.body[bodyParsingKey]) {
            reject(`Error - Key ${bodyParsingKey} not found in response`);
          }
          resolve(bodyParsingKey ? result.body[bodyParsingKey] : result);
        }
      });
    });
  }
}

module.exports = function () {
  return new Request();
};
