const URL = require('url'); // TODO avoid using node url module  to maintain universal usage
const Request = require('./base-request');


const DEFAULT_PORT = 443;
const DEFAULT_SCHEME = 'https';

module.exports.builder = (accessToken, apiUrl) => {
  const url = URL.parse(apiUrl);
  const port = url.port === '' || url.port === null ? DEFAULT_PORT : Number(url.port);
  const host = url.hostname + url.pathname;
  const scheme = DEFAULT_SCHEME; // Treezor only uses HTTPS

  return Request()
    .setHost(host)
    .setPort(port)
    .setScheme(scheme)
    .setAuth(accessToken);
};
