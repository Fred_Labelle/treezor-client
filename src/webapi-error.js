function WebapiError(message, status, statusCode, url, method) {
  this.name = 'WebapiError';
  this.message = message || '';
  this.statusCode = status;
  this.url = url;
  this.method = method;
}

WebapiError.prototype = Error.prototype;

module.exports = WebapiError;
