const ApiMethods = require('./api-methods');

function WebApiClient(config) {
  let _config = config || {};
  const apiMethods = new ApiMethods(_config.accessToken, _config.apiUrl);

  function setConfigKey(configKey, value) {
    _config = _config || {};
    _config[configKey] = value;
  }

  function getConfigKey(configKey) {
    if (!_config) {
      return;
    }
    return _config[configKey];
  }

  function resetConfigKey(configKey) {
    if (!_config) {
      return;
    }
    _config[configKey] = null;
  }

  const baseMethods = {
    setConfig: (newConfig) => {
      Object.keys(newConfig).forEach((key) => {
        if (Object.prototype.hasOwnProperty.call(newConfig, key)) {
          _config[key] = newConfig[key];
        }
      });
    },

    getConfig: () => _config,

    resetConfig: () => {
      _config = null;
    },

    setAccessToken: (accessToken) => {
      setConfigKey('accessToken', accessToken);
    },

    setApiURL: (apiUrl) => {
      setConfigKey('apiUrl', apiUrl);
    },

    getApiURL: () => getConfigKey('apiUrl'),

    getAccessToken: () => getConfigKey('accessToken'),

    resetAccessToken: () => {
      resetConfigKey('accessToken');
    },

    resetApiURL: () => {
      resetConfigKey('apiUrl');
    },
  };

  return Object.assign(baseMethods, apiMethods);
}

module.exports = WebApiClient;
