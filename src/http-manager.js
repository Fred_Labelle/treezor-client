const superagent = require('superagent');
const WebApiError = require('./webapi-error');

const HttpManager = {};

/* Create superagent options from the base request */
const getParametersFromRequest = (request) => {
  const options = {};

  if (request.getQueryParameters()) {
    options.query = request.getQueryParameters();
  }

  if (
    request.getHeaders()
    && request.getHeaders()['Content-Type'] === 'application/json'
  ) {
    options.data = JSON.stringify(request.getBodyParameters());
  } else if (request.getBodyParameters()) {
    options.data = request.getBodyParameters();
  }

  if (request.getHeaders()) {
    options.headers = request.getHeaders();
  }
  return options;
};

/* Create an error object from an error returned from the Web API */
const getErrorObject = (defaultMessage, err) => {
  let errorObject;
  if (
    typeof err.error === 'object'
    && err.error.response !== undefined
    && typeof err.error.response.text === 'string'
  ) {
    // Web API Error format
    errorObject = new WebApiError(
      err.error.response.text,
      err.error.status,
      err.error.statusCode,
      err.url,
      err.method,
    );
  } else if (typeof err.error === 'string') {
    // Authorization Error format
    errorObject = new WebApiError(`${err.error}: ${err.error_description}`);
  } else if (typeof err === 'string') {
    // Serialized JSON error
    try {
      const parsedError = JSON.parse(err);
      errorObject = new WebApiError(
        parsedError.error.message,
        parsedError.error.status,
      );
    } catch (error) {
      // Error not JSON formatted
      console.error(error);
    }
  }

  if (!errorObject) {
    // Unexpected format
    errorObject = new WebApiError(`${defaultMessage}: ${JSON.stringify(err)}`);
  }

  return errorObject;
};

/* Make the request to the Web API */
HttpManager.makeRequest = (method, options, uri, callback, bodyParsingKey) => {
  const req = method(uri);
  if (options.query) {
    req.query(options.query);
  }

  if (
    options.data
    && (!options.headers || options.headers['Content-Type'] !== 'application/json')
  ) {
    req.type('form');
    req.send(options.data);
  } else if (options.data) {
    req.send(options.data);
  }

  if (options.headers) {
    req.set(options.headers);
  }

  req.end((err, response) => {
    if (err) {
      const errorObject = getErrorObject('Request error', {
        error: err,
        url: req.url,
        method: req.method,
      });
      return callback(errorObject);
    }
    if(bodyParsingKey) {
      const result = response.body[bodyParsingKey];
      if(!result) {
        const errorObject = getErrorObject(`Key ${bodyParsingKey} not found in response`, {
          error: err,
          url: req.url,
          method: req.method,
        });
        return callback(errorObject);
      }
      return callback(null, result);
    }
    const result = {
        body: response.body,
        headers: response.headers,
        statusCode: response.statusCode,
      };
    return callback(null, result);
  });
};

/**
 * Make a HTTP GET request.
 * @param {BaseRequest} The request.
 * @param {Function} The callback function.
 * @param {String} The key used to parse body response.
 */
HttpManager.get = (request, callback, bodyParsingKey) => {
  const options = getParametersFromRequest(request);
  const method = superagent.get;

  HttpManager.makeRequest(method, options, request.getURI(), callback, bodyParsingKey);
};

/**
 * Make a HTTP POST request.
 * @param {BaseRequest} The request.
 * @param {Function} The callback function.
 */
HttpManager.post = (request, callback) => {
  const options = getParametersFromRequest(request);
  const method = superagent.post;

  HttpManager.makeRequest(method, options, request.getURI(), callback);
};

/**
 * Make a HTTP DELETE request.
 * @param {BaseRequest} The request.
 * @param {Function} The callback function.
 */
HttpManager.del = (request, callback) => {
  const options = getParametersFromRequest(request);
  const method = superagent.del;

  HttpManager.makeRequest(method, options, request.getURI(), callback);
};

/**
 * Make a HTTP PUT request.
 * @param {BaseRequest} The request.
 * @param {Function} The callback function.
 */
HttpManager.put = (request, callback) => {
  const options = getParametersFromRequest(request);
  const method = superagent.put;

  HttpManager.makeRequest(method, options, request.getURI(), callback);
};

module.exports = HttpManager;
