# JS Treezor Client

This library is an HTTP universal javascript client on top of [Treezor Web API](https://www.treezor.com/api-documentation/) Web API.

It is a universal module, it means it can be runned from a server (node.js) or within a browser.

## Getting Started 🚀

NB: you first need first to get API credentials from Treezor to start using it.

To start, simply add the module:

```bash
npm install js-treezor-client
```

Then set it up with your credentials:

```Javascript
const TreezorClient = require('js-treezor-client');

const client = new TreezorClient({
  apiUrl: 'https://sandbox.treezor.com/v1/index.php',
  accessToken: 'YOUR_TOKEN_DELIVERED_BY_TREEZOR', // Store it securely ;)
});
```

And you are good to go. For instance, here is an example to retrieve all the users on your account:

```Javascript
client.getUsers()
.then(result => console.log(result))
.catch(error => console.log(error));
```

To check more methods available, just check the folder /src/api-methods.

That's all ;)

### With async/await

You may instead use async/await :

```javascript
const TreezorClient = require('js-treezor-client');

const client = new TreezorClient({
  apiUrl: 'https://sandbox.treezor.com/v1/index.php',
  accessToken: 'YOUR_TOKEN_DELIVERED_BY_TREEZOR',
});

const getUsers = async () => {
  try {
    const result = await client.getUsers();
    console.log(result);
  } catch(error) {
    console.log(error);
  }
}
```

### Callback

You may specify an optionnal callback function, like so:

```javascript
client.getWallets((err, result) => console.log(result));
```

## Contributions 🎡

I used to work on a project based on Treezor API. And it was not simple everyday :)
So I wanted to share this work to help others go faster in integrating the API and concentrate on what matters.
It does not cover the whole API, and it surely needs some improvements and updates (I do not work on the project anymore and some API methods are not yet implemented in this module).

So feel free to contribute, that may help others in the future too ;)

### Testing

A test example using Jest framework is available in the folder /tests. It illustrates the validation of an endpoint against a Json schema.

After cloning this repository, from the root folder:

```bash
yarn install
```

Replace the 'accessToken' by your own token in the test file .

Finally, laucnh tests:

```bash
jest
```

Some endpoints are documented with [JsonSchema](https://json-schema.org/), which is a pretty useful format to validate Json.

These schemas do not replace the official documentation but may be usefull to complete it (in case you want to add missing information or test endpoints accuracy for instance). Feel free to enrich them while if you use this library.
