module.exports = {
  id: '/transfer',
  title: 'Transfer model',
  type: 'object',
  properties: {
    transferId: { type: 'string' },
    transferTypeId: {
      type: 'string',
      enum: ['1', '2', '3', '4'],
      description:
        '1	Wallet to wallet - 2	Card transaction - 3	Client fees - 4	Credit note',
    },
    transferTag: { type: 'string' },
    transferStatus: {
      type: 'string',
      enum: ['VALIDATED', 'CANCELED', 'PENDING'],
    },
    walletId: { type: 'string' },
    foreignId: { type: 'string' },
    walletTypeId: {
      type: 'string',
      enum: ['9', '10', '14'],
      description:
        '9	Electronic Money Wallet - 10	Payment Account Wallet - 14	Electronic Money Card (Internal only)',
    },
    beneficiaryWalletId: { type: 'string' },
    beneficiaryWalletTypeId: { type: 'string' },
    transferDate: { type: 'string' },
    amount: { type: 'string' },
    currency: {
      type: 'string',
      description: 'ISO 4217',
      enum: ['EUR'],
    },
    label: { type: 'string' },
    partnerFee: { type: 'string' },
    createdDate: { type: 'string' },
    modifiedDate: { type: 'string' },
    walletEventName: { type: 'string' },
    walletAlias: {
      type: 'string',
      description: 'Deprecated - ingore',
    },
    beneficiaryWalletEventName: { type: 'string' },
    beneficiaryWalletAlias: { type: 'string' },
    codeStatus: { type: 'string' },
    informationStatus: { type: 'string' },
  },
  required: [
    'transferId',
    'transferTypeId',
    'transferStatus',
    'walletId',
    'beneficiaryWalletId',
    'transferDate',
    'amount',
    'currency',
  ],
};
