const transactionSchema = require('./transaction');

module.exports = {
  title: 'Transactions',
  id: '/getTransactions',
  type: 'array',
  items: transactionSchema,
};
