const transferSchema = require('./transfer');

module.exports = {
  title: 'Transfers',
  id: '/getTransfers',
  type: 'object',
  type: 'array',
  items: transferSchema,
};
