const walletSchema = require('./wallet');

module.exports = {
  title: 'Wallets',
  id: '/getWallets',
  type: 'array',
  items: walletSchema,
};
