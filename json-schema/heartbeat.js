module.exports = {
  title: 'Server Heartbeat',
  type: 'object',
  properties: {
    body: {
      type: 'string',
    },
    statusCode: {
      type: 'number',
      enum: [200],
    },
  },
  required: ['statusCode', 'body'],
};
