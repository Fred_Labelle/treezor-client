module.exports = {
  id: '/transaction',
  title: 'Transaction',
  type: 'object',
  properties: {
    transactionId: { type: 'string' },
    walletDebitId: { type: 'string' },
    walletCreditId: { type: 'string' },
    transactionType: {
      type: 'string',
      enum: [
        'Payin',
        'Payout',
        'Transfer',
        'Transfer Refund',
        'Payin Refund',
        'Discount',
        'Bill',
      ],
    },
    foreignId: {
      type: 'string',
      description:
        'ID of the ressource relative to this transaction:  payin, payout, transfer etc',
    },
    name: {
      type: 'string',
      description: 'TODO',
    },
    description: {
      type: 'string',
      description: 'TODO - diff with name ?',
    },
    valueDate: { type: 'string' },
    executionDate: { type: 'string' },
    amount: { type: 'string' },
    walletDebitBalance: {
      type: 'string',
      description: 'current balance of debited wallet',
    },
    walletCreditBalance: {
      type: 'string',
      description: 'current balance of credited wallet',
    },
    currency: {
      type: 'string',
      enum: ['EUR'],
    },
    createdDate: { type: 'string' },
  },
  required: ['transactionId'],
};
