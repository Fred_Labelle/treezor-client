const userSchema = require('./user');

module.exports = {
  title: 'Users',
  id: '/getUsers',
  type: 'array',
  items: userSchema,
};
