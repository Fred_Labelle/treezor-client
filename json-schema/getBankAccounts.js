const bankAccountSchema = require('./bank-account');

module.exports = {
  title: 'BankAccounts',
  id: '/getBankAccounts',
  type: 'array',
  items: bankAccountSchema,
};
