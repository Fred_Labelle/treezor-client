module.export = {
  id: '/user',
  title: 'User',
  type: 'object',
  properties: {
    userId: { type: 'string' },
    userTypeId: {
      type: 'string',
      enum: ['1', '2'],
      description: '1 personne physique - 2 personne morale',
    },
    userStatus: { type: 'string' },
    clientId: { type: 'string' },
    userTag: { type: 'string' },
    parentUserId: { type: 'string' },
    parentType: { type: 'string' },
    title: { type: 'string' },
    firstname: { type: 'string' },
    lastname: { type: 'string' },
    middleNames: { type: 'string' },
    birthday: { type: 'string' },
    email: { type: 'string' },
    address1: { type: 'string' },
    address2: { type: 'string' },
    postcode: { type: 'string' },
    city: { type: 'string' },
    state: { type: 'string' },
    country: { type: 'string' },
    countryName: { type: 'string' },
    phone: { type: 'string' },
    mobile: { type: 'string' },
    nationality: { type: 'string' },
    nationalityOther: { type: 'string' },
    placeOfBirth: { type: 'string' },
    birthCountry: { type: 'string' },
    occupation: { type: 'string' },
    incomeRange: { type: 'string' },
    legalName: { type: 'string' },
    legalNameEmbossed: { type: 'string' },
    legalRegistrationNumber: { type: 'string' },
    legalTvaNumber: { type: 'string' },
    legalRegistrationDate: { type: 'string' },
    legalForm: { type: 'string' },
    legalShareCapital: { type: 'string' },
    legalSector: { type: 'string' },
    legalAnnualTurnOver: { type: 'string' },
    legalNetIncomeRange: { type: 'string' },
    legalNumberOfEmployeeRange: { type: 'string' },
    effectiveBeneficiary: { type: 'string' },
    kycLevel: { type: 'string' },
    kycReview: { type: 'string' },
    kycReviewComment: { type: 'string' },
    isFreezed: { type: 'string' },
    createdDate: { type: 'string' },
    modifiedDate: { type: 'string' },
    codeStatus: { type: 'string' },
    informationStatus: { type: 'string' },
    sepaCreditorIdentifier: { type: 'string' },
    walletCount: { type: 'string' },
    payinCount: { type: 'string' },
  },
  required: [
    'userId',
    'userTypeId',
    'userStatus',
    'clientId',
    'userTag',
    'title',
    'firstname',
    'lastname',
    'middleNames',
    'birthday',
    'email',
    'address1',
    'address2',
    'postcode',
    'city',
    'state',
    'country',
    'countryName',
    'phone',
    'mobile',
    'nationality',
    'nationalityOther',
    'placeOfBirth',
    'birthCountry',
    'occupation',
    'incomeRange',
    'legalName',
    'legalNameEmbossed',
    'legalRegistrationNumber',
    'legalTvaNumber',
    'legalRegistrationDate',
    'legalForm',
    'legalShareCapital',
    'legalSector',
    'legalAnnualTurnOver',
    'legalNetIncomeRange',
    'legalNumberOfEmployeeRange',
    'effectiveBeneficiary',
    'kycLevel',
    'kycReview',
    'kycReviewComment',
    'isFreezed',
    'createdDate',
    'modifiedDate',
    'codeStatus',
    'informationStatus',
    'sepaCreditorIdentifier',
    'walletCount',
    'payinCount',
  ],
};
