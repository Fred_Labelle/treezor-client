// DEPRECATED - use Beneficiaries instead

module.exports = {
  id: '/bankAccount',
  title: 'Bank Account model',
  type: 'object',
  properties: {
    bankaccountId: { type: 'string' },
    bankaccountTag: { type: 'string' },
    userId: { type: 'string' },
    bankaccountType: {
      type: 'string',
      description: 'TODO values',
    },
    bankaccountStatus: {
      type: 'string',
      enum: ['VALIDATED', 'CANCELED', 'PENDING'],
    },
    bankaccountOwnerName: { type: 'string' },
    bankaccountOwnerAddress: { type: 'string' },
    bankaccountIBAN: { type: 'string' },
    bankaccountBIC: { type: 'string' },
    createdDate: { type: 'string' },
    modifiedDate: { type: 'string' },
    name: { type: 'string' },
    codeStatus: {
      type: 'string',
      description: 'TODO values',
    },
    informationStatus: {
      type: 'string',
      description: 'TODO values',
    },
  },
  required: [
    'bankaccountId',
    'userId',
    'bankaccountTag',
    'bankaccountType',
    'bankaccountStatus',
    'bankaccountIBAN',
    'bankaccountBIC',
    'createdDate',
    'modifiedDate',
    'codeStatus',
    'informationStatus',
  ],
};
