const payinSchema = require('./payin');

module.exports = {
  title: 'Payins',
  id: '/getPayins',
  type: 'array',
  items: payinSchema,
};
