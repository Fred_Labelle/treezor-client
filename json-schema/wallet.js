module.exports = {
  id: '/wallet',
  title: 'Wallet model',
  type: 'object',
  properties: {
    walletId: { type: 'string' },
    walletTypeId: {
      type: 'string',
      enum: ['9', '10', '14'],
      description:
        '9	Electronic Money Wallet - 10	Payment Account Wallet - 14	Electronic Money Card (Internal only)',
    },
    walletStatus: {
      type: 'string',
      enum: ['VALIDATED', 'CANCELED', 'PENDING'],
    },
    userId: { type: 'string' },
    jointUserId: {
      type: 'string',
      description: 'Deprecated - ignore',
    },
    walletTag: {
      type: 'string',
      description: 'Open field',
    },
    currency: {
      type: 'string',
      enum: ['EUR'],
    },
    eventDate: {
      type: 'string',
      description: 'Deprecated - ignore',
    },
    eventMessage: {
      type: 'string',
      description: 'Deprecated - ignore',
    },
    eventAlias: {
      type: 'string',
      description: 'Deprecated - ignore',
    },
    eventPayinStartDate: {
      type: 'string',
      description: 'Deprecated - ignore',
    },
    eventPayinEndDate: {
      type: 'string',
      description: 'Deprecated - ignore',
    },
    contractSigned: {
      type: 'string',
      description: 'Deprecated - ignore',
    },
    urlImage: { type: 'string' },
    createdDate: { type: 'string' },
    modifiedDate: { type: 'string' },
    tariffId: { type: 'string' },
    eventName: { type: 'string' },
    userFirstname: { type: 'string' },
    userLastname: { type: 'string' },
    codeStatus: { type: 'string' },
    informationStatus: { type: 'string' },
    payinCount: {
      type: 'string',
      description: 'Number of payins done on the wallet',
    },
    payoutCount: {
      type: 'string',
      description: 'Number of payouts done on the wallet',
    },
    transferCount: {
      type: 'string',
      description: 'Number of transfers done on the wallet',
    },
    solde: {
      type: ['string', null],
      description: 'Wallet current balance',
    },
    authorizedBalance: {
      type: ['string', null],
      description:
        'Amount available, all ongoing transactions are substracted.',
    },
    bic: { type: 'string' },
    iban: { type: 'string' },
  },
  required: [
    'walletId',
    'walletTypeId',
    'walletStatus',
    'userId',
    'currency',
    'createdDate',
    'modifiedDate',
    'solde',
    'authorizedBalance',
    'bic',
    'iban',
  ],
};
