const payoutSchema = require('./payout');

module.exports = {
  title: 'Payouts',
  id: '/getPayouts',
  type: 'object',
  type: 'array',
  items: payoutSchema,
};
