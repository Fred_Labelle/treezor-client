const TreezorClient = require('../index');
const _ = require('lodash');
const { validate } = require('jsonschema');
const heartBeatSchema = require('../json-schema/heartbeat');
const walletsSchema = require('../json-schema/getWallets');

const API_KEY = '34750bf146b034b389b9315eefc84a41b506bbf3';
const API_URL = 'https://sandbox.treezor.com/v1/index.php';

let treezorClient = {};

function setUpTreezorClient() {
  treezorClient = new TreezorClient({
    apiUrl: API_URL,
    accessToken: API_KEY,
  });
}


expect.extend({
  toBeValidSchema(received, schema) {
    const result = validate(received, schema);
    const { errors } = result;
    const pass = errors.length < 1;
    const message = pass
      ? () => `Valid schema\n`
      : () => {
          const errorMessage = _.reduce(errors, (output, value, key) => {
            return output + `\n${value.stack}`;
          });
          return (
            `Not valid json schema: \n\n` +
            `Expected schema to be :\n` +
            `  ${this.utils.printExpected(schema)}\n` +
            `Error details :\n` +
            `${errorMessage}\n` +
            `Received:\n` +
            ` ${this.utils.printReceived(received)}`
          );
        };

    return { actual: received, message, pass };
  },
});

beforeAll(() => {
  if(!API_URL || !API_KEY) throw 'Missing Treezor URL or KEY'
  return setUpTreezorClient();
});


test('The server heartbeat is responding ', async () => {
  expect.assertions(1);
  await expect(treezorClient.heartBeat()).resolves.toBeValidSchema(
    heartBeatSchema,
  );
});

// This test is an example of validating an API endpoint's response against JSON schema
test('GET all user wallets ', async () => {
  expect.assertions(1);
  await expect(treezorClient.getWallets()).resolves.toBeValidSchema(
    walletsSchema,
  );
});
